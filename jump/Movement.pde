class MovementData {
    private int px;
    private int py;
    private float vy;

    private int check_px;
    private int check_py;

    private boolean cheetah;

    private float speed_mod;

    private float dx;
    private float dy;
    private boolean allow_jump;
    private final float g;
    private final float jump_speed;

    public MovementData(int px, int py, float g, float jump_speed) {
        this.check_px = this.px = px;
        this.check_py = this.py = py;
        this.dx = 0;
        this.dy = 0;
        this.vy = 0;
        this.g = g;
        this.allow_jump = false;
        this.jump_speed = jump_speed;
        this.speed_mod = 1;
        this.cheetah = false;
    }

    private boolean go_x, go_y;

    public void apply_move(MapData level) {
        if(cheetah) {
            px += dx;
            py += dy;
            dx = dy = 0;
            return;
        }
        float c = height / current_level.h;
        go_x = go_y = true;
        dx *= speed_mod;
        level.entries.forEach(entry -> {
            if(entry.c == '\0') return;
            if(entry.x * c < px + dx && px + dx < (entry.x + 1.5) * c && py >= (entry.y - 1) * c && py <= (entry.y + 0.5) * c) {
                go_x &= canStepThrough(entry.c);
            }
            if(entry.x * c < px && px < (entry.x + 1.5) * c && py + vy >= (entry.y - 1) * c && py + vy <= (entry.y + 0.5) * c) {
                if(py + vy > entry.y * c && !canStepThrough(entry.c)) {
                    vy = abs(vy);
                } else go_y &= canStepThrough(entry.c);
            }
        });
        if(go_x) {
            px += dx;
        }
        if(go_y) {
            py += vy;
            allow_jump = false;
        } else {
            vy = 0;
            allow_jump = true;
        }
        speed_mod = 1;
        vy += g;
        dx = 0;
    }

    public void jump() {
        if(allow_jump) vy -= jump_speed;
    }

    public void schedule_x_move(float d) {
        dx += d;
    }

    public void schedule_y_move(float d) {
        dy += d;
    }

    public void set_y_speed(float d) {
        vy = d;
    }

    public void set_position(int x, int y) {
        px = x;
        py = y;
    }

    public int get_px() {
        return px;
    }

    public int get_py() {
        return py;
    }

    public void set_check(int x, int y) {
        check_px = x;
        check_py = y;
    }

    public void restore_check() {
        px = check_px;
        py = check_py;
        vy = 0;
    }

    public int get_check_x() {
        return check_px;
    }

    public int get_check_y() {
        return check_py;
    }

    public void set_speed_mod(float n) {
        speed_mod = n;
    }

    public void toggle_cheat_mode() {
        cheetah = !cheetah;
    }

    public boolean is_cheating() {
        return cheetah;
    }
}
