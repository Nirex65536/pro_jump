private MapData current_level;
private MovementData p_state;
private Textures textures;
private LevelSelector selector;
private EffectHandler effects;

private int c = 0;

void setup() {
    size(1920, 1080, JAVA2D);
    stroke(#aaaaaa);

    frameRate(80);

    textures = new Textures();
    selector = new LevelSelector();
    effects = new EffectHandler();
}

private int ppx;
private int xoffset = 0;

private final boolean pressed[] = new boolean[256];

void draw() {
    background(#222222);

    if(selector.selecting()) {
        selector.render();
        return;
    }

    c = height / current_level.h;
    if(pressed[LEFT]) {
        p_state.schedule_x_move(-8f);
    }
    if(pressed[RIGHT]) {
        p_state.schedule_x_move(8f);
    }
    if(pressed[DOWN]) {
        p_state.schedule_y_move(8f);
    }
    if(pressed[UP]) {
        p_state.schedule_y_move(-8f);
    }
    if(pressed[' ']) {
        p_state.jump();
    }

    if(p_state.get_px() < width / 2) {
        ppx = p_state.get_px();
        xoffset = 0;
    }
    else if(p_state.get_px() > current_level.w * c - width / 2) {
        ppx = p_state.get_px() - current_level.w * c + width;
        xoffset = current_level.w * c - width;
    }
    else {
        ppx = width / 2;
        xoffset = p_state.get_px() - width / 2;
    }

    current_level.entries.forEach(entry -> {
        fill(getBlockColor(entry.c));
        if(entry.x * c < p_state.get_px() && p_state.get_px() < (entry.x + 1.5) * c) {
            if(p_state.py + p_state.vy >= (entry.y - 1) * c && p_state.py + p_state.vy <= (entry.y + 0.5) * c) { // this is ugly...i should definitely get rid of this
                entry.special.ifPresent(special -> {
                    switch(special.type) {
                        case 'E':
                            effects.create(special.args.get(0).charAt(0), Integer.parseInt(special.args.get(1)), Float.parseFloat(special.args.get(2)));
                            break;
                        case 'P': 
                            p_state.set_position(Integer.parseInt(special.args.get(0)) * c + 3 * c / 4, Integer.parseInt(special.args.get(1)) * c - 1);
                            if(special.args.get(2).equals("False")) {
                                p_state.set_y_speed(0);
                            }
                            break;
                        case 'S': 
                            p_state.set_y_speed(Float.parseFloat(special.args.get(0)) * -1);
                            break;
                        case 'D':
                            die(special.args.get(0).equals("True"));
                            break;
                        case 'C':
                            p_state.set_check(Integer.parseInt(special.args.get(0)) * c + 3 * c / 4, Integer.parseInt(special.args.get(1)) * c - 1);
                            break;
                        case 'I':
                            p_state.set_speed_mod(Float.parseFloat(special.args.get(0)));
                            break;
                        case 'T':
                            fill(#ffffff);
                            textAlign(CENTER);
                            textSize(Integer.parseInt(special.args.get(2)));
                            text(special.args.get(0).replaceAll("`", " "), width/2, Integer.parseInt(special.args.get(1)));
                            break;
                    }
                });
                textures.setTexture(entry.c);
                if(entry.c == BlockType.GOAL) win();
            }
        }
        if(entry.c == '\0') return;
        if (entry.c == BlockType.GHOST) return;
        if(entry.x * c > p_state.get_px() + width || entry.x * c + c < p_state.get_px() - width) return;
        rect((entry.x) * c - xoffset, entry.y * c, c, c);
    });

    if(p_state.py > current_level.h * c) p_state.restore_check();

    image(textures.getCurrentTexture(), ppx - c / 2, (p_state.py + c / 2), c / 2, c / 2);

    effects.tick(current_level, p_state);
    p_state.apply_move(current_level);
}

void die(boolean really) {
    if(p_state.is_cheating()) return;
    if(!current_level.keep_effects) effects.reset();
    if(really) exit();
    p_state.restore_check();
}

private boolean cheated = false;
void win() {
    println("you won!");
    if(cheated) {
        println("(Using cheats...)");
    }
    exit();
}

void keyReleased() {
    if (keyCode > 255) return;
    pressed[keyCode] = false;
}

void keyPressed() {
    if (keyCode > 255) return;
    if(selector.selecting()) {
        switch(keyCode) {
            case RIGHT:
                selector.increase_selection();
                break;
            case LEFT:
                selector.decrease_selection();
                break;
            case ENTER:
                current_level = selector.run();
                p_state = new MovementData((int)((current_level.sx + 0.75) * height / current_level.h), current_level.sy * height / current_level.h - 1, 0.4, 15);
                println("Running " + current_level.title + " ...");
                break;
        }
    } else if (keyCode == 'R') {
        readMapData(current_level.filename).ifPresentOrElse(entry -> {
            println("successfully reloaded level: " + current_level.filename);
            current_level = entry;
        }, () -> {
            println("failed to reload level: " + current_level.filename);
       });
    } else if(keyCode == 'C') {
        p_state.toggle_cheat_mode();
        cheated = true;
    } else {
        pressed[keyCode] = true;
    }
}
