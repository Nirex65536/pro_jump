module Block where

import Prelude hiding (tail, head)
import Data.Char
import Data.Bifunctor

data Entry = Entry {
    name :: String,
    char :: Char,
    color :: String,
    clipping :: Bool,
    texture :: String
}

-- Parsing

tail :: [a] -> [a]
tail (x:xs) = xs
tail _ = []

head :: String -> Char
head (x:xs) = x
head [] = '\x00'

parseChar :: String -> Char
parseChar = head . takeWhile (/= '\'') . tail . dropWhile (/= '\'')

parseString :: String -> String
parseString = takeWhile (/= '"') . tail . dropWhile (/= '"')

getPart :: Int -> String -> String
getPart i s = if i == 0 then dropWhile isSpace (fst x) else getPart (i - 1) (snd x) where x = span (/= ',') $ dropWhile isSpace $ dropWhile (== ',') $ dropWhile isSpace s

parseEntry :: String -> Entry
parseEntry s = Entry {
    name        = parseString $ getPart 0 s,
    char        = parseChar $ getPart 1 s,
    color       = parseString $ getPart 2 s,
    clipping    = getPart 3 s == "True",
    texture     = parseString $ getPart 4 s
} 

fetchEntry :: String -> (String, String)
fetchEntry = second tail . span (/= '}') . tail . dropWhile (/= '{')

parseAll :: String -> [Entry]
parseAll [] = []
parseAll s = if fst x /= "" then (parseEntry . fst) x : parseAll (snd  x) else [] where x = fetchEntry s

-- Code Generation

genClassEntry :: [Entry] -> String
genClassEntry (e:es) = "    public static final char " ++ name e ++ " = " ++ show (char e) ++ ";\n" ++ genClassEntry es
genClassEntry [] = []

genClass :: [Entry] -> String
genClass e = "static class BlockType {\n" ++ genClassEntry e ++ "}\n"

genColorEntry :: [Entry] -> String
genColorEntry (e:es) = "        case BlockType." ++ name e ++ ":\n" ++ "            return " ++ color e ++ ";\n" ++ genColorEntry es
genColorEntry [] = []

genColor :: [Entry] -> String
genColor e = "int getBlockColor(char type) {\n    switch(type) {\n" ++ genColorEntry e ++ "        default:\n            return #00ff00;\n    }\n}\n"

genClipEntry :: [Entry] -> String
genClipEntry = foldr (\e -> (++) (if clipping e then "        case BlockType." ++ name e ++ ":\n" else [])) []

genClip :: [Entry] -> String
genClip e = "boolean canStepThrough(char type) {\n    switch(type) {\n" ++ genClipEntry e ++ "            return true;\n        default:\n            return false;\n    }\n}"

genTexEntry :: [Entry] -> String
genTexEntry (e:es) = if texture e == "" then genTexEntry es else "        textures.put(" ++ show (char e) ++ ", loadImage(\"assets/textures/" ++ texture e ++ ".png" ++ "\"));\n" ++ genTexEntry es
genTexEntry [] = []

genTex :: [Entry] -> String
genTex e = "// Automatically generated file, do not edit!\n\nimport java.util.Map;\nimport java.util.HashMap;\n\nclass Textures {\n    private Map<Character, PImage> textures;\n    private PImage current;\n\n    public Textures() {\n        textures = new HashMap<>();\n        current = loadImage(\"assets/textures/normal.png\");\n" ++ genTexEntry e ++ "    }\n\n    public PImage getCurrentTexture() {\n        return current;\n    }\n\n    public void setTexture(char block) {\n        if(textures.containsKey(block)) current = textures.get(block);\n    }\n}"

genAll :: [Entry] -> String
genAll e = "// Automatically generated file, do not edit!\n\n" ++ genClass e ++ "\n" ++ genColor e ++ "\n" ++ genClip e

main :: IO ()
main = readFile "blocks.fl" >>= (\x -> do 
    writeFile "Block.pde" $ genAll x
    writeFile "Textures.pde" $ genTex x) . parseAll
